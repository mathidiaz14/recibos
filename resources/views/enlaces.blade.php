@extends('layouts.app')

@section('content')

    <section class="p-t-20">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="table-data__tool">
                    	<div class="table-data__tool-left">
                    		<h3 class="title-5 m-b-35">Listado de enlaces del edificio {{$recibo->edificio->nombre}}</h3>
                    	</div>
                    </div>
                    @include('ayuda.alerta')
                    <div class="table-responsive table-responsive-data2">
                        <table class="table table-data2">
                            <thead>
                                <tr>
                                	<th>Unidad</th>
                                    <th>Enlace</th>
                                    <th>Eliminar</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($enlaces as $enlace)
                                	<tr class="tr-shadow">
	                                    <td>{{$enlace->unidad}}</td>
                                        <td>
                                        	<a href="{{$enlace->link}}" target="_blank" class="btn btn-success">
                                        		<i class="fa fa-eye"></i>
                                        		Abrir enlace
                                        	</a>
                                        </td>
                                        <td>
                                            @include('ayuda.eliminar', ['id' => $enlace->id, 'ruta' => url('enlace', $enlace->id)])
	                                    </td>
	                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-md-12" style="margin-top:15px;">
                    {{$enlaces->links()}}
                </div>
            </div>
        </div>
    </section>
    <!-- END DATA TABLE-->
@endsection

@section('js')
	
@endsection