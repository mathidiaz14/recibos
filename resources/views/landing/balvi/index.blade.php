@extends('landing.balvi.master')

@section('content')
    <!-- Banner -->
        <section id="banner" style="height:45vh; min-height:40vh;">
            <div class="inner">
                <h2>¡Busca tu edificio!</h2>
                <p>Accede a toda la información del condominio</p>
            </div>
            <a href="#one" class="more scrolly">Continuar</a>
        </section>

    <!-- One -->
        <section id="one" class="wrapper style1 special">
            <div class="inner">
                <header class="major">
                    <form action="{{url('buscar')}}" method="GET" class="form-horizontal">
                        <h2>Busca tu edificio</h2>
                        <div class="col-md-6">    
                            <input type="text" class="form-group" name="edificio" id="edificio" placeholder="Ingresa aquí el nombre o la dirección de tu edificio">
                        </div>
                        <br>
                        <div class="col-md-6">
                            <button class="button primary" onclick="$('.cargando').fadeIn();">
                                <i class="fa fa-search"></i>
                                Buscar
                            </button>
                        </div>
                    </form>
                </header>
            </div>
        </section>
@endsection

@section('js')
    <script>
        $(document).ready(function()
        {
            $('#edificio').focus();
        });
    </script>
@endsection
