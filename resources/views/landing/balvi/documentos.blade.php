@extends('landing.syg.master')

@section('content')
    <!-- Banner -->
        <section id="banner" style="height:40vh!important; min-height:30vh!important;">
            <div class="inner">
                <h2>{{$edificio->nombre}}</h2>
                <a href="#one" class="more scrolly">Ver mes</a>
            </div>
        </section>

    <!-- One -->
        <section id="one" class="wrapper style1 special">
            <div class="inner">
                <header class="major">
                    <h2>Documentos</h2>
                    
                    <div class="row contenedor_home">
                    	<div class="col-12">
                    		<a class="button primary btn_actas" style="width:100%!important;">
		                        Actas de asamblea
		                    </a>
                    	</div>

	                    <div class="col-12">
	                    	<a class="button primary btn_reglamentos" style="width:100%!important;">
		                        Reglamento
		                    </a>
	                    </div>

	                    <div class="col-12">
	                    	<a class="button primary btn_polizas" style="width:100%!important;">
		                        Polizas de seguro
		                    </a>
	                    </div>

	                    <div class="col-12">
	                    	<a class="button primary btn_comprobantes" style="width:100%!important;">
		                        Comprobantes
		                    </a>
	                    </div>

	                    <div class="col-12">
		    				<form action="{{url('get_menu')}}" method="post">
                				@csrf
                				<input type="hidden" name="edificio" value="{{$edificio->id}}">
                				<button class="button" style="margin-top:2em; width: 100%!important;">
	                				<i class="fa fa-chevron-left"></i>
		                			Atras
		                		</button>
                			</form>
		    			</div>
                    </div>

                    @php
                    	$tipos = ['acta', 'reglamento', 'poliza', 'comprobante'];
                    @endphp

                    @foreach($tipos as $tipo)
                    	 <div class="row contenedor" id="{{$tipo}}" style="display:none;">
	                		@if($edificio->documentos->where('tipo', $tipo)->count() == 0)
	                			<div class="col-12">
	                				<p>No hay ningun documento de este tipo</p>
	                			</div>
	                		@endif
	                		@foreach($edificio->documentos->where('tipo', $tipo) as $documento)
	                    		<div class="col-12">
			                    	<a class="button primary" href="{{url('descargar_documento', $documento->id)}}" style="width:100%!important;">
			                            <i class="fa fa-file-alt"></i>
			                            {{$documento->nombre}}
			                        </a>
	                    		</div>
		                    @endforeach
		                    <div class="col-12">
		                    	<br>
		                    	<a class="button btn_back" style="width:100%!important;">
		                    		<i class="fa fa-chevron-left"></i>
		                			Atras
		                    	</a>
		                    </div>
	                    </div>
                    @endforeach

                </header>
            </div>
        </section>

    

@endsection

@section('js')
	<script>
		$(document).ready(function()
		{
			$('.btn_actas').click(function()
			{
				$('.contenedor_home').hide();
				$('.contenedor').hide();
				$('#acta').fadeIn();
			});

			$('.btn_reglamentos').click(function()
			{
				$('.contenedor_home').hide();
				$('.contenedor').hide();
				$('#reglamento').fadeIn();
			});

			$('.btn_polizas').click(function()
			{
				$('.contenedor_home').hide();
				$('.contenedor').hide();
				$('#poliza').fadeIn();
			});

			$('.btn_comprobantes').click(function()
			{
				$('.contenedor_home').hide();
				$('.contenedor').hide();
				$('#comprobante').fadeIn();
			});

			$('.btn_back').click(function()
			{
				$('.contenedor').hide();
				$('.contenedor_home').fadeIn();
			});
		});
	</script>
@endsection
