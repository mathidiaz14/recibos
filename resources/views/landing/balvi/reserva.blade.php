@extends('landing.balvi.master')

@section('content')
    <!-- Banner -->
        <section id="banner" style="height:40vh!important; min-height:30vh!important;">
            <div class="inner">
                <h2>{{$edificio->nombre}}</h2>
                <a href="#one" class="more scrolly">Ver mes</a>
            </div>
        </section>

    <!-- One -->
        <section id="one" class="wrapper style1 special">
            <div class="inner">
                <header class="major">
                    <h2>Reserva de espacios comunes</h2>
                    @if(Session::has('exito'))
                    	<h3 style="color:#008d00;">
                    		{{Session::get('exito')}}
                    		{{Session::forget('exito')}}
                    	</h3>
                    @endif
                    <form action="{{url('reserva')}}" method="post" class="form-horizontal" style="text-align: left;">
                    	@csrf
                    	<input type="hidden" name="edificio" value="{{$edificio->id}}">
                    	<div class="row">
                    		<div class="form-group col-6">
	                    		<label for="">Fecha para los próximos 30 días </label>
	                    		<select name="fecha" id="fecha" class="form-control">
	                    			<option>Fecha</option>
	                    			@php
										$date 	= \Carbon\Carbon::today();
							    	@endphp

							    	@for($i = 0; $i <= 30; $i++)
										<option value="{{$date->format('d-m-Y')}}">
						    				{{$date->format('d/m/Y')}}
						    			</option>

							    		@php
											$date->addDay();
							    		@endphp
							    	@endfor
	                    		</select>
	                    	</div>
	                    	<div class="form-group col-6">
	                    		<label for="">Turno</label>
						    	@php
									$day 	= \Carbon\Carbon::today();
						    	@endphp

						    	@for($i = 0; $i <= 30; $i++)

	                    			<select name="inicio" id="inicio_{{$day->format('d-m-Y')}}" class="horas form-control" disabled @if($i > 0) style="display:none;" @endif>

	                    				@php
	                    					$matutino = $edificio->reservas->where('fecha', $day)->where('inicio', 'matutino')->where('estado', '!=', 'rechazado')->first();

	                    					$nocturno = $edificio->reservas->where('fecha', $day)->where('inicio', 'nocturno')->where('estado', '!=', 'rechazado')->first();
	                    				@endphp

	                    				@if($matutino)
											<option disabled>
								    			Diurno ({{$matutino->estado == 'aprobado' ? "Reservado" : "Pendiente aprobación"}})
							    			</option>
							    		@else
							    			<option value="matutino" id="{{$day->format('d-m-Y')}}_matutino">
								    			Diurno
							    			</option>
							    		@endif

						    			@if($nocturno)
											<option disabled>
								    			Nocturno ({{$nocturno->estado == 'aprobado' ? "Reservado" : "Pendiente aprobación"}})
							    			</option>
							    		@else
							    			<option value="nocturno" id="{{$day->format('d-m-Y')}}_nocturno">
								    			Nocturno
							    			</option>
							    		@endif
	                    			</select>
	                    		
	                    			@php
										$day->addDay();
						    		@endphp
						    	@endfor
	                    	</div>
	                    	<div class="form-group col-6" style="margin-top:20px;">
	                    		<label for="">Unidad</label>
	                    		<input type="text" class="form-control" name="unidad" placeholder="Unidad" required>
	                    	</div>
	                    	<div class="form-group col-6" style="margin-top:20px;">
	                    		<label for="">Nombre</label>
	                    		<input type="text" class="form-control" name="nombre" placeholder="Nombre" required>
	                    	</div>
	                    	<div class="form-group col-12" style="margin-top:20px;">
	                    		<label for="">Email</label>
	                    		<input type="email" class="form-control" name="email" placeholder="Email" required>
	                    	</div>
	                    	<div class="form-group col-12" style="margin-top:30px; text-align: center;">
	                    		<button class="button primary" style="width:100%!important;" disabled>
	                    			<i class="fa fa-paper-plane"></i>
	                    			Enviar
	                    		</button>
	                    	</div>
                    	</div>
                    </form>
                	<br><br>
                	<div class="col-12 text-center">
                		<form action="{{url('get_menu')}}" method="post">
            				@csrf
            				<input type="hidden" name="edificio" value="{{$edificio->id}}">
            				<button class="button" style="width: 100%!important;">
                				<i class="fa fa-chevron-left"></i>
	                			Atras
	                		</button>
            			</form>
                	</div>
                </header>
            </div>
        </section>

    

@endsection

@section('js')
	<script>
		$(document).ready(function()
		{	
			$('#fecha').change(function()
			{
				if($('#fecha option:selected').val() != "Fecha")
				{
					var fecha = $('#fecha option:selected').val();

					$(".horas").hide();
					$('.horas').prop('required', true);
					
					$('#inicio_'+fecha).show();
					$('#inicio_'+fecha).attr('disabled', false);
					$('#inicio_'+fecha).prop('required', true);
					
					$('#horas').attr('disabled', false);
					$('button').attr('disabled', false);
				}else
				{
					$('#inicio').attr('disabled', true);
					$('#horas').attr('disabled', true);
					$('button').attr('disabled', true);
				}
			});
		});
	</script>
@endsection

