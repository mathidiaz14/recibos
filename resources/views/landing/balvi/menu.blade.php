@extends('landing.balvi.master')

@section('content')
    <!-- Banner -->
        <section id="banner" style="height:40vh!important; min-height:30vh!important;">
            <div class="inner">
                <h2>{{$edificio->nombre}}</h2>
                <a href="#one" class="more scrolly">Ver mes</a>
            </div>
        </section>

    <!-- One -->
        <section id="one" class="wrapper style1 special">
            <div class="inner">
                <header class="major">
                    <h2>Menu</h2>
                    <form action="{{url('fechas')}}" method="post" class="form-horizontal">
                    	@csrf
                    	<input type="hidden" name="edificio" value="{{$edificio->id}}">
                        <button class="button primary" style="width:100%!important;" onclick="$('.cargando').fadeIn();">
                            <i class="fa fa-file-alt"></i>
                            Liquidaciones
                        </button>
                    </form>

                    <form action="{{url('get_documentos')}}" method="post" class="form-horizontal">
                    	@csrf
                    	<input type="hidden" name="edificio" value="{{$edificio->id}}">
                        <button class="button primary" style="width:100%!important;" onclick="$('.cargando').fadeIn();">
                            <i class="fa fa-folder"></i>
                            Documentos
                        </button>
                    </form>

                    @if($edificio->contrasena_admin == null)
                        <form action="" method="post" class="form-horizontal">
                        	@csrf
                        	<input type="hidden" name="edificio" value="{{$edificio->id}}">
                            <button class="button primary disabled" style="width:100%!important;" disabled>
                                <i class="fa fa-calendar"></i>
                                Reserva espacios comunes
                            </button>
                        </form>
                    @else
                        <form action="{{url('get_reserva')}}" method="post" class="form-horizontal">
                            @csrf
                            <input type="hidden" name="edificio" value="{{$edificio->id}}">
                            <button class="button primary" style="width:100%!important;">
                                <i class="fa fa-calendar"></i>
                                Reserva espacios comunes
                            </button>
                        </form>
                    @endif

                    <a href="{{url('/')}}" class="button" style="width:100%!important;">
                        <i class="fa fa-sign-out-alt"></i>
                        Salir
                    </a>
                </header>
            </div>
        </section>

    

@endsection

@section('js')

@endsection
