@extends('landing.balvi.master')

@section('content')
    <!-- Banner -->
        <section id="banner" style="height:40vh!important; min-height:30vh!important;">
            <div class="inner">
                <h2>Mes de liquidación</h2>
            </div>
            <a href="#one" class="more scrolly">Ver mes</a>
        </section>

    <!-- One -->
        <section id="one" class="wrapper style1 special">
            <div class="inner">
                <header class="major" id="fechas">
					<h2>{{$edificio->nombre}}</h2>

    				<div class="fecha_grupo">
						<p>Seleccione la fecha del recibo</p>
        				<div class="col-4">
                			@foreach($edificio->recibos as $recibo)
		        				<a class="button primary btn_fecha" attr-fecha="{{$recibo->fecha}}">
		        					@php $fecha = explode('-', $recibo->fecha); @endphp
	                                {{$fecha[1]}}/{{$fecha[0]}}
		        				</a>
        					@endforeach
	        			</div>
	    				<div class="col-12">
		    				<form action="{{url('get_menu')}}" method="post">
                				@csrf
                				<input type="hidden" name="edificio" value="{{$edificio->id}}">
                				<button class="button" style="margin-top:2em;">
	                				<i class="fa fa-chevron-left"></i>
		                			Atras
		                		</button>
                			</form>
		    			</div>
        			</div>
        			
        			@foreach($edificio->recibos as $recibo)
            			<div class="fechas" id="fecha_{{$recibo->fecha}}" style="display: none;">
	            			<p>Selecciona la unidad</p>
            				
            				@if($edificio->liquidaciones->where('fecha', $recibo->fecha)->first() != null)
	            				<div class="row">
	            					<div class="col-12">
	            						<a href="{{url('descargar_liquidacion', $edificio->liquidaciones->where('fecha', $recibo->fecha)->first()->id)}}" class="button primary">
			                				Descargar liquidación
			                			</a>
	            					</div>
	            				</div>
            				@endif
	                		<div class="row">
		                		@foreach($recibo->enlaces as $enlace)
		                			<div class="col-4">
		                				<a href="{{url($enlace->link)}}" target="_blank" class="button primary" style="width: 100%;">
			                				{{$enlace->unidad}}
			                			</a>
		                			</div>
		                		@endforeach
		                	</div>
		                	<div class="row" style="margin-top:2em;">
		                		<div class="col-12">
		                			<a class="button btn_atras">
			                			<i class="fa fa-chevron-left"></i>
			                			Atras
			                		</a>
		                		</div>
		                	</div>
	            		</div>
	    			@endforeach
                </header>
            </div>
        </section>

@endsection

@section('js')
	<script>
		$(document).ready(function()
		{
			$('.btn_fecha').click(function()
			{
				var fecha = $(this).attr('attr-fecha');
				
				$('.fecha_grupo').fadeOut();
				$('#fecha_'+fecha).fadeIn();
			});

			$('.btn_atras').click(function()
			{
				$('.fechas').fadeOut();
				$('.fecha_grupo').fadeIn();
			});
		});
	</script>
@endsection
