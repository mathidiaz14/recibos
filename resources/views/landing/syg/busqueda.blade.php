@extends('landing.syg.master')

@section('content')
    <!-- Banner -->
        <section id="banner" style="height:40vh!important; min-height:30vh!important;">
            <div class="inner">
                <h2>Resultados de la búsqueda</h2>
            </div>
            <a href="#one" class="more scrolly">Ver resultado</a>
        </section>

    <!-- One -->
        <section id="one" class="wrapper style1 special">
        	
        	@if(session()->has('contrasena'))
        		<div class="alerta " role="alert">
			  		{{session('contrasena')}}
					{{session()->forget('contrasena')}}
				</div>
        	@endif
        	
            <header class="major" id="edificios">
        		<p>¿Cuál de estos es tu edificio?</p>
            	<div class="row">
	                @foreach($edificios as $edificio)
	                	<div class="col-4">
		                	<a class="button primary btn_edificio" attr-edificio="{{$edificio->id}}" style="width: 100%;">
			                    {{$edificio->nombre}}
			                </a>
			            </div>
	                @endforeach
	                <div class="col-12">
		                <a href="{{url('/')}}" class="button">
		                	<i class="fa fa-chevron-left"></i>
		                	Volver
		                </a>
		            </div>
	            	</div>
            </header>
			<div class="inner">
	            @foreach($edificios as $edificio)
	            	<header class="major edificios" id="edificio_{{$edificio->id}}" style="display: none;">
	            		<h2>{{$edificio->nombre}}</h2>

	            		<form action="{{url('menu')}}" method="POST">
	            			@csrf
	            			<input type="hidden" name="edificio" value="{{$edificio->id}}">
	            			
	            			@if($edificio->contrasena != null)
	                			<div class="contrasena">
	                				<p>Ingrese la contraseña del edificio</p>

			            			<input type="password" class="form-control" name="contrasena" id="contrasena_{{$edificio->id}}">
			            			<br>
			            			<button class="button primary" onclick="$('.cargando').fadeIn();">
			            				<i class="fa fa-key"></i>
			            				Ingresar
			            			</button>
	                			</div>
	                		@else
	                			<button href="" class="button primary">
	                				<i class="fa fa-paper-plane"></i>
	                				Ingresar
	                			</button>
	                		@endif
	            		</form>
	            		<a class="button btn_back">
	            			<i class="fa fa-chevron-left"></i>
		                	Atras
		                </a>
	            	</header>
	            @endforeach
            </div>
        </section>

@endsection

@section('js')
	<script>
		$(document).ready(function()
		{
			$('.btn_edificio').click(function()
			{
				var id = $(this).attr('attr-edificio');

				$('#edificios').fadeOut();
				$('#edificio_'+id).fadeIn();
				$('#contrasena_'+id).focus();
			});

			$('.btn_back').click(function()
			{
				$('.edificios').fadeOut();
				$('#edificios').fadeIn();
			});
		});
	</script>
@endsection
