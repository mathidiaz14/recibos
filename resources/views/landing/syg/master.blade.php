<!DOCTYPE HTML>
<html>
    <head>
        <title>{{env('APP_TITULO')}}</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
        <link rel="stylesheet" href="{{asset('landing/css/main.css')}}" />
        <noscript><link rel="stylesheet" href="{{asset('landing/css/noscript.css')}}" /></noscript>
        <link rel="icon" type="image/png" href="{{asset('favicon.ico')}}">

        <style>
            .wrapper.style1 {
                background-color: #A0B6CC!important;
                color: #fff!important;
            }

            .wrapper.style1 header.major p {
                color: #000000;
            }

            .button.primary
            {
                background-color: #0562AE!important;
            }

            .button.primary:hover
            {
                background-color: #002c50!important;
            }

            .wrapper.style1 ::-webkit-input-placeholder {
                color: #fff !important;
            }

            input[type="text"]:focus, input[type="password"]:focus, input[type="email"]:focus, select:focus, textarea:focus {
                box-shadow: 0 0 0 2px #0562ae9c;
            }

        </style>
        <script src="{{asset('landing/js/jquery.min.js')}}"></script>
    </head>
    <body class="landing is-preload">
        <style>
            .cargando
            {
                position: fixed;
                top: 0px;
                left: 0px;
                z-index: 2050;
                width: 100%;
                height: 100%;
                background: #0000009e;
                text-align: center;
            }

            // Ring
            .lds-ring {
              display: inline-block;
              position: relative;
              width: 64px;
              height: 64px;
            }
            .lds-ring div {
              box-sizing: border-box;
              display: block;
              position: absolute;
              width: 51px;
              height: 51px;
              margin: 6px;
              border: 6px solid #0562AE;
              border-radius: 50%;
              animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
              border-color: #0562AE transparent transparent transparent;
            }
            .lds-ring div:nth-child(1) {
              animation-delay: -0.45s;
            }
            .lds-ring div:nth-child(2) {
              animation-delay: -0.3s;
            }
            .lds-ring div:nth-child(3) {
              animation-delay: -0.15s;
            }
            @keyframes lds-ring {
              0% {
                transform: rotate(0deg);
              }
              100% {
                transform: rotate(360deg);
              }
            }
        </style>

        <div class="cargando" style="display: none;">
            <div class="lds-ring">
                <div></div><div></div><div></div><div></div>
            </div>
        </div>  

        <script>
            $('.lds-ring').css("position","absolute");
            $('.lds-ring').css("top", Math.max(0, (($(window).height() - $($('.lds-ring')).outerHeight()) / 2) + $(window).scrollTop()) + "px");
            $('.lds-ring').css("left", Math.max(0, (($(window).width() - $($('.lds-ring')).outerWidth()) / 2) + $(window).scrollLeft()) + "px");
        </script>
        <!-- Page Wrapper -->
            <div id="page-wrapper">
                <!-- Header -->
                <header id="header" >
                    <h1><a href="http://www.sygadministraciones.com.uy">{{env('APP_TITULO')}}</a></h1>
                </header>

            	@yield('content')
                <!-- Footer -->
                <footer id="footer">
                    <ul class="icons">
                        <li><a href="{{url('home')}}" class="button">Acceder al dashboard</a></li>
                    </ul>
                    <ul class="copyright">
                        <li>Design by: <a href="http://mathiasdiaz.uy">Mathias Díaz.uy</a></li>
                    </ul>
                </footer>
            </div>

        <!-- Scripts -->
            <script src="{{asset('landing/js/jquery.scrollex.min.js')}}"></script>
            <script src="{{asset('landing/js/jquery.scrolly.min.js')}}"></script>
            <script src="{{asset('landing/js/browser.min.js')}}"></script>
            <script src="{{asset('landing/js/breakpoints.min.js')}}"></script>
            <script src="{{asset('landing/js/util.js')}}"></script>
            <script src="{{asset('landing/js/main.js')}}"></script>

            @yield('js')
    </body>
</html>