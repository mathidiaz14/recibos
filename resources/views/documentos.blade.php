@extends('layouts.app')

@section('content')
    
    <section class="p-t-20">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="table-data__tool">
                    	<div class="table-data__tool-left">
                            <h3 class="title-5 m-b-35">Listado de documentos de {{$edificio->nombre}}</h3>
                    	</div>
                        <div class="table-data__tool-right">
                        	<button type="button" class="au-btn au-btn-icon au-btn--blue au-btn--small" data-toggle="modal" data-target="#agregardocumento">
                                <i class="fa fa-plus"></i>
                                Agregar archivo documento
                            </button>

                            <!-- Modal -->
                            <div class="modal fade" id="agregardocumento" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Agregar archivo</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <div class="modal-body">
                                    <form action="{{url('documento')}}" method="post" class="form-horizontal" enctype="multipart/form-data">
                                        @csrf
                                        <input type="hidden" name="edificio" value="{{$edificio->id}}">
                                        <div class="form-group">
                                            <label for="archivo" class=" form-control-label">Archivo</label>
                                            <input required="" type="file" id="archivo" placeholder="Ingrese aqui el archivo" name="archivo" class="form-control">
                                        </div>
                                        <div class="form-group">
                                        	<label for="">Tipo</label>
                                        	<select name="tipo" id="tipo" class="form-control">
	                                        	<option value="acta">Acta</option>
	                                        	<option value="reglamento">Reglamento</option>
	                                        	<option value="poliza">Poliza</option>
                                                <option value="comprobante">Comprobante</option>
	                                        </select>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                                <i class="fa fa-chevron-left"></i>
                                                Cerrar
                                            </button>
                                            <button class="btn btn-primary">
                                                <i class="fa fa-save"></i>
                                                Guardar
                                            </button>
                                        </div>
                                    </form>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <a href="{{url('documento', $edificio->id + 1)}}" class="au-btn au-btn-icon au-btn--green au-btn--small">
                                <fa class="fa fa-chevron-right"></fa>
                            </a>
                        </div>
                    </div>
                    @include('ayuda.alerta')
                    <div class="table-responsive table-responsive-data2">
                        <table class="table table-data2">
                            <thead>
                                <tr>
                                    <th>Archivo</th>
                                    <th>Tipo</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($edificio->documentos->sortByDesc('created_at') as $documento)
                                	<tr class="tr-shadow">
                                        <td>
                                        	{{$documento->nombre}}
                                        </td>
                                        <td>
                                        	{{$documento->tipo}}
                                        </td>
                                        <td>
                                            <a href="{{url('descargar_documento', $documento->id)}}" class="btn btn-primary">
                                                <i class="fa fa-archive"></i>
                                                Descargar
                                            </a>
                                        </td>   
	                                    
                                        <td>
                                            @include('ayuda.eliminar', ['id' => $documento->id, 'ruta' => url('documento', $documento->id)])
	                                    </td>
	                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END DATA TABLE-->
@endsection

@section('js')
	<script>
		
	</script>			
@endsection