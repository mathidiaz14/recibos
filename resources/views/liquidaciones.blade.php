@extends('layouts.app')

@section('content')
    
    <section class="p-t-20">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="table-data__tool">
                    	<div class="table-data__tool-left">
                            <h3 class="title-5 m-b-35">Listado de liquidaciones de {{$edificio->nombre}}</h3>
                    	</div>
                        <div class="table-data__tool-right">
                        	<button type="button" class="au-btn au-btn-icon au-btn--blue au-btn--small" data-toggle="modal" data-target="#agregarLiquidacion">
                                <i class="fa fa-plus"></i>
                                Agregar archivo liquidacion
                            </button>

                            <!-- Modal -->
                            <div class="modal fade" id="agregarLiquidacion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Agregar archivo</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <div class="modal-body">
                                    <form action="{{url('liquidacion')}}" method="post" class="form-horizontal" enctype="multipart/form-data">
                                        @csrf
                                        <input type="hidden" name="edificio" value="{{$edificio->id}}">
                                        <div class="form-group">
                                            <label for="fecha" class=" form-control-label">Fecha</label>
                                            <input required="" type="month" id="fecha" placeholder="Ingrese aqui la fecha" name="fecha" class="form-control" value="{{Carbon\Carbon::now()->subMonth()->format('Y-m')}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="archivo" class=" form-control-label">Archivo</label>
                                            <input required="" type="file" id="archivo" placeholder="Ingrese aqui el archivo" name="archivo" class="form-control">
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                                <i class="fa fa-chevron-left"></i>
                                                Cerrar
                                            </button>
                                            <button class="btn btn-primary">
                                                <i class="fa fa-save"></i>
                                                Guardar
                                            </button>
                                        </div>
                                    </form>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <a href="{{url('liquidacion', $edificio->id + 1)}}" class="au-btn au-btn-icon au-btn--green au-btn--small">
                                <fa class="fa fa-chevron-right"></fa>
                            </a>
                        </div>
                    </div>
                    @include('ayuda.alerta')
                    <div class="table-responsive table-responsive-data2">
                        <table class="table table-data2">
                            <thead>
                                <tr>
                                    <th>Fecha</th>
                                    <th>Archivo</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($edificio->liquidaciones->sortByDesc('created_at') as $liquidacion)
                                	<tr class="tr-shadow">
	                                    <td>
                                            @php $fecha = explode('-', $liquidacion->fecha); @endphp
                                            {{$fecha[1]}}/{{$fecha[0]}}   
                                        </td>
                                        <td>
                                            <a href="{{url('descargar_liquidacion', $liquidacion->id)}}" class="btn btn-primary">
                                                <i class="fa fa-archive"></i>
                                                Descargar
                                            </a>
                                        </td>   
	                                    
                                        <td>
                                            @include('ayuda.eliminar', ['id' => $liquidacion->id, 'ruta' => url('liquidacion', $liquidacion->id)])
	                                    </td>
	                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END DATA TABLE-->
@endsection

@section('js')
	<script>
		
	</script>			
@endsection