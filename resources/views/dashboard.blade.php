<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>{{env('APP_TITULO')}}</title>

    <!-- Fontfaces CSS-->
    <link href="{{asset('css/font-face.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('vendor/font-awesome-4.7/css/font-awesome.min.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('vendor/font-awesome-5/css/fontawesome-all.min.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('vendor/mdi-font/css/material-design-iconic-font.min.css')}}" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="{{asset('vendor/bootstrap-4.1/bootstrap.min.css')}}" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="{{asset('vendor/animsition/animsition.min.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('vendor/wow/animate.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('vendor/css-hamburgers/hamburgers.min.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('vendor/slick/slick.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('vendor/select2/select2.min.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('vendor/perfect-scrollbar/perfect-scrollbar.css')}}" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="{{asset('css/theme.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('css/style.css')}}" rel="stylesheet" media="all">
    

    <link rel="icon" type="image/png" href="{{asset('favicon.png')}}">

</head>

<body class="animsition">
    <div class="page-wrapper">
        <!-- HEADER DESKTOP-->
        <header class="header-desktop3 d-none d-lg-block">
            <div class="section__content section__content--p35">
                <div class="header3-wrap">
                    <div class="header__logo">
                        <a href="{{url('/')}}" style="color:white;">
                            <b>{{env('APP_TITULO')}}</b>
                        </a>
                    </div>
                    <div class="header__tool">
                        <div class="account-wrap">
                            <div class="account-item account-item--style2 clearfix js-item-menu">
                                <div class="content">
                                    <a class="js-acc-btn" href="#">Menú</a>
                                </div>
                                <div class="account-dropdown js-dropdown">
                                    <div class="account-dropdown__footer">    
                                        <a  href="{{ url('/') }}">
                                            <i class="zmdi zmdi-power"></i>Cerrar Sesión</a>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- END HEADER DESKTOP-->

        <!-- HEADER MOBILE-->
        <header class="header-mobile header-mobile-2 d-block d-lg-none">
            <div class="header-mobile__bar">
                <div class="container-fluid">
                    <div class="header-mobile-inner">
                        <a href="{{url('/')}}" style="color:white;">
                            <img src="{{asset('images/logo.png')}}" alt="CoolAdmin" width="50px" /> <b>{{env('APP_TITULO')}}</b>
                        </a>
                        <button class="hamburger hamburger--slider" type="button">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
        </header>

        <div class="sub-header-mobile-2 d-block d-lg-none">
            <div class="header__tool">
                <div class="account-wrap">
                    <div class="account-item account-item--style2 clearfix js-item-menu">
                       <div class="content">
                            <a class="js-acc-btn" href="#">Menú</a>
                        </div>
                        <div class="account-dropdown js-dropdown">
                            <div class="account-dropdown__footer">    
                                <a  href="{{ url('/') }}">
                                    <i class="zmdi zmdi-power"></i>Cerrar Sesión</a>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END HEADER MOBILE -->

        <!-- PAGE CONTENT-->
        <div class="page-content--bgf7" style="min-height: 100vh;">
            <br>
            
            <section class="statistic statistic2">
		        <div class="container">
		            <div class="row">
                        <div class="col-12">
                            <form action="{{url('pdf')}}" class="form-horizontal" method="post">
                                @csrf
                                <input type="hidden" name="edificio" value="{{$edificio->id}}">
                                <div class="row">
                                    <div class="col-3">
                                        <input type="date" class="form-control" name="inicio" required>
                                    </div>
                                    <div class="col-3">
                                        <input type="date" class="form-control" name="fin" required>
                                    </div>
                                    <div class="col-3">
                                        <select name="estado" id="" class="form-control">
                                            <option value="aprobado">Aprobados</option>
                                            <option value="pendiente">Pendientes</option>
                                            <option value="rechazado">Rechazados</option>
                                            <option value="todos">Todos</option>
                                        </select>
                                    </div>
                                    <div class="col-3">
                                        <button class="btn btn-info btn-block">
                                            Generar PDF
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
		            	<div class="col-12 mt-3">
		            		@if(Session::has('exito'))
		            			<div class="alert alert-success">
		            				<p>{{Session::get('exito')}}</p>
		            				{{Session::forget('exito')}}
		            			</div>
		            		@endif
		            		<h3>Reservas pendientes</h3>
		            		<hr>
		            		<br>
		            		<div class="table table-resposinve">
		            			<table class="table table-striped">
		            				<tr>
                                        <th>Fecha</th>
		            					<th>Turno</th>
		            					<th>Unidad</th>
		            					<th>Nombre</th>
                                        <th>Teléfono</th>
		            					<th></th>
		            					<th></th>
		            				</tr>
		            				@foreach($edificio->reservas->where('estado', 'pendiente') as $reserva)
		            					<tr>
                                            <td>{{$reserva->fecha->format('d/m/Y')}}</td>
			            					<td>{{$reserva->inicio == "matutino" ? "Diurno" : "Nocturno"}}</td>
			            					<td>{{$reserva->unidad}}</td>
			            					<td>{{$reserva->nombre}}</td>
                                            <td>{{$reserva->telefono}}</td>
			            					<td>
			            						<form action="{{url('reserva/aprobar', $reserva->id)}}" method="post">
			            							@csrf
			            							<button class="btn btn-success">
				            							Aprobar
				            						</butoon>
			            						</form>
			            					</td>
			            					<td>
                                                <button class="btn btn-danger" data-toggle="modal" data-target="#rechazar_{{$reserva->id}}">
                                                    Rechazar
                                                </button>

                                                <!-- Modal -->
                                                <div class="modal fade" id="rechazar_{{$reserva->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true" >
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header" style="background: #dc3545;">
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body text-left">
                                                                <p>Deja un comentario para el inquilino</p>
                                                                <br>
                                                                <form action="{{url('reserva/rechazar', $reserva->id)}}" method="post">
                                                                    @csrf
                                                                    <textarea name="comentario" id="" cols="30" rows="3" class="form-control" placeholder="Deja aquí tu comentario" required></textarea>
                                                                    <hr>
                                                                    <div class="row">
                                                                        <div class="col">
                                                                            <button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">
                                                                                Atras
                                                                            </button>
                                                                        </div>
                                                                        <div class="col">               
                                                                            <button class="btn btn-danger btn-block">
                                                                                Rechazar
                                                                            </butoon>
                                                                        </div>
                                                                    </div>
                                                                </form>   
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
			            					</td>
		            					</tr>
		            				@endforeach
		            			</table>
		            		</div>		
		            	</div>
		            	<div class="col-12">
		            		<br>
		            		<hr>
		            	</div>
		            	<div class="col-12">
		            		<h3>Reservas anteriores</h3>
		            		<hr>
		            		<br>
		            		<div class="table table-resposinve">
		            			<table class="table table-striped">
		            				<tr>
                                        <th>Fecha</th>
		            					<th>Turno</th>
		            					<th>Unidad</th>
		            					<th>Nombre</th>
                                        <th>Teléfono</th>
		            					<th>Estado</th>
                                        <th>Comentario</th>
                                        <th></th>
                                        <th></th>
		            				</tr>
		            				@foreach($edificio->reservas->where('estado', '!=', 'pendiente')->sortByDesc('fecha') as $reserva)
		            					<tr>
			            					<td>{{$reserva->fecha->format('d/m/Y')}}</td>
			            					<td>{{$reserva->inicio == "matutino" ? "Diurno" : "Nocturno"}}</td>
			            					<td>{{$reserva->unidad}}</td>
			            					<td>{{$reserva->nombre}}</td>
                                            <td>{{$reserva->teléfono}}</td>
			            					<td>{{$reserva->estado}}</td>
                                            <td>{{$reserva->comentario == null ? "--" : $reserva->comentario}}</td>
                                            <td>
                                                @if((\Carbon\Carbon::now()->lt($reserva->fecha)) and ($reserva->estado == "rechazado"))
                                                    <form action="{{url('reserva/aprobar', $reserva->id)}}" method="post">
                                                        @csrf
                                                        <button class="btn btn-success">
                                                            Aprobar
                                                        </butoon>
                                                    </form>
                                                @else
                                                    <a class="btn btn-success disabled" disabled>
                                                        Aprobar
                                                    </a>
                                                @endif
                                            </td>
                                            <td>
                                                @if((\Carbon\Carbon::now()->lt($reserva->fecha)) and ($reserva->estado == "aprobado"))
                                                    <button class="btn btn-danger" data-toggle="modal" data-target="#rechazar_{{$reserva->id}}">
                                                        Rechazar
                                                    </button>

                                                    <!-- Modal -->
                                                    <div class="modal fade" id="rechazar_{{$reserva->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true" >
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header" style="background: #dc3545;">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body text-left">
                                                                    <p>Deja un comentario para el inquilino</p>
                                                                    <br>
                                                                    <form action="{{url('reserva/rechazar', $reserva->id)}}" method="post">
                                                                        @csrf
                                                                        <textarea name="comentario" id="" cols="30" rows="3" class="form-control" placeholder="Deja aquí tu comentario" required></textarea>
                                                                        <hr>
                                                                        <div class="row">
                                                                            <div class="col">
                                                                                <button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">
                                                                                    Atras
                                                                                </button>
                                                                            </div>
                                                                            <div class="col">               
                                                                                <button class="btn btn-danger btn-block">
                                                                                    Rechazar
                                                                                </butoon>
                                                                            </div>
                                                                        </div>
                                                                    </form>   
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @else
                                                    <a class="btn btn-danger disabled" disabled>
                                                        Rechazar
                                                    </a>
                                                @endif
                                            </td>
		            					</tr>
		            				@endforeach
		            			</table>
		            		</div>		
		            	</div>
		            </div>
	           	</div>
	        </section>
        </div>
    </div>

    <!-- Jquery JS-->
    <script src="{{asset('vendor/jquery-3.2.1.min.js')}}"></script>
    <!-- Bootstrap JS-->
    <script src="{{asset('vendor/bootstrap-4.1/popper.min.js')}}"></script>
    <script src="{{asset('vendor/bootstrap-4.1/bootstrap.min.js')}}"></script>
    <!-- Vendor JS -->
    <script src="{{asset('vendor/slick/slick.min.js')}}">
    </script>
    <script src="{{asset('vendor/wow/wow.min.js')}}"></script>
    <script src="{{asset('vendor/animsition/animsition.min.js')}}"></script>
    <script src="{{asset('vendor/bootstrap-progressbar/bootstrap-progressbar.min.js')}}">
    </script>
    <script src="{{asset('vendor/counter-up/jquery.waypoints.min.js')}}"></script>
    <script src="{{asset('vendor/counter-up/jquery.counterup.min.js')}}">
    </script>
    <script src="{{asset('vendor/circle-progress/circle-progress.min.js')}}"></script>
    <script src="{{asset('vendor/perfect-scrollbar/perfect-scrollbar.js')}}"></script>
    <script src="{{asset('vendor/chartjs/Chart.bundle.min.js')}}"></script>
    <script src="{{asset('vendor/select2/select2.min.js')}}">
    </script>

    <!-- Main JS-->
    <script src="{{asset('js/main.js')}}"></script>

    @yield('js')

</body>

</html>
<!-- end document-->

