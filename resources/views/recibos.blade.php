@extends('layouts.app')

@section('content')

    <section class="p-t-20">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="table-data__tool">
                    	<div class="table-data__tool-left">
                    		<h3 class="title-5 m-b-35">Listado de recibos</h3>
                    	</div>
                        <div class="table-data__tool-right">
                        	<button type="button" class="au-btn au-btn-icon au-btn--green au-btn--small" data-toggle="modal" data-target="#agregarEdificio">
							  	<i class="fa fa-plus"></i>
							  	Agregar archivo de recibos
							</button>

							<!-- Modal -->
							<div class="modal fade" id="agregarEdificio" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							  <div class="modal-dialog" role="document">
							    <div class="modal-content">
							      <div class="modal-header">
							        <h5 class="modal-title" id="exampleModalLabel">Agregar archivo</h5>
							        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
							          <span aria-hidden="true">&times;</span>
							        </button>
							      </div>
							      <div class="modal-body">
							        <form action="{{url('recibo')}}" method="post" class="form-horizontal" enctype="multipart/form-data">
							        	@csrf
							        	<div class="form-group">
							        		<label for="fecha" class=" form-control-label">Fecha</label>
                                            <input required="" type="month" id="fecha" placeholder="Ingrese aqui la fecha" name="fecha" class="form-control">
							        	</div>
							        	<div class="form-group">
                                            <label for="archivo" class=" form-control-label">Archivo</label>
                                            <input required="" type="file" id="archivo" placeholder="Ingrese aqui el archivo" name="archivo" class="form-control">
                                        </div>
                                        <div class="modal-footer">
									        <button type="button" class="btn btn-secondary" data-dismiss="modal">
									        	<i class="fa fa-chevron-left"></i>
									        	Cerrar
									        </button>
									        <button class="btn btn-primary">
									        	<i class="fa fa-save"></i>
									        	Guardar
									        </button>
								      	</div>
							        </form>
							      </div>
							    </div>
							  </div>
							</div>
                        </div>
                    </div>
                    @include('ayuda.alerta')
                    <div class="table-responsive table-responsive-data2">
                        <table class="table table-data2">
                            <thead>
                                <tr>
                                	<th>Codigo</th>
                                    <th>Edificio</th>
                                    <th>Fecha</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach(App\Models\Recibos::all() as $recibo)
                                	<tr class="tr-shadow">
                                		<td>{{$recibo->edificio->codigo}}</td>
	                                    <td>{{$recibo->edificio->nombre}}</td>
	                                    <td>
                                            @php $fecha = explode('-', $recibo->fecha); @endphp
                                            {{$fecha[1]}}/{{$fecha[0]}}
                                        </td>
                                        <td>
                                        	<a href="{{url('recibo', $recibo->id)}}" class="btn btn-success">
                                        		<i class="fa fa-eye"></i>
                                        		Ver enlaces
                                        	</a>
                                        </td>
                                        <td>
                                            @include('ayuda.eliminar', ['id' => $recibo->id, 'ruta' => url('recibo', $recibo->id)])
	                                    </td>
	                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END DATA TABLE-->
@endsection

@section('js')
	<script>
		$('#agregarEdificio').on('shown.bs.modal', function () {
		  $('#codigo').trigger('focus');
		});
	</script>			
@endsection