@extends('layouts.app')

@section('content')

    <section class="p-t-20">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="table-data__tool">
                    	<div class="table-data__tool-left">
                    		<h3 class="title-5 m-b-35">Listado de recibos del edificio {{$edificio->nombre}}</h3>
                    	</div>
                        <div class="table-data__tool-right">
                        	
                        </div>
                    </div>
                    @include('ayuda.alerta')
                    <div class="table-responsive table-responsive-data2">
                        <table class="table table-data2">
                            <thead>
                                <tr>
                                    <th>Fecha</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($edificio->recibos as $recibo)
                                	<tr class="tr-shadow">
	                                    <td>
                                            @php $fecha = explode('-', $recibo->fecha); @endphp
                                            {{$fecha[1]}}/{{$fecha[0]}}
                                        </td>
                                        <td>
                                        	<a href="{{url('recibo', $recibo->id)}}" class="btn btn-success">
                                        		<i class="fa fa-eye"></i>
                                        		Ver enlaces
                                        	</a>
                                        </td>
                                        <td>
                                            @include('ayuda.eliminar', ['id' => $recibo->id, 'ruta' => url('recibo', $recibo->id)])
	                                    </td>
	                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END DATA TABLE-->
@endsection

@section('js')
	<script>
		$('#agregarEdificio').on('shown.bs.modal', function () {
		  $('#codigo').trigger('focus');
		});
	</script>			
@endsection