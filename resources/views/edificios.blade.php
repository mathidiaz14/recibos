@extends('layouts.app')

@section('content')
    <section class="p-t-20">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="table-data__tool">
                    	<div class="table-data__tool-left">
                            <h3 class="title-5 m-b-35">Listado de edificios ({{$edificios->count()}})</h3>
                    	</div>
                        <div class="table-data__tool-right">
                            <button type="button" class="au-btn au-btn-icon au-btn--blue au-btn--small" data-toggle="modal" data-target="#agregarArchivo">
                                <i class="fa fa-plus"></i>
                                Recibos
                            </button>

                            <!-- Modal -->
                            <div class="modal fade" id="agregarArchivo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Agregar archivo</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <div class="modal-body">
                                    <form action="{{url('recibo')}}" method="post" class="form-horizontal" enctype="multipart/form-data">
                                        @csrf
                                        <div class="form-group">
                                            <label for="fecha" class=" form-control-label">Fecha</label>
                                            <input required="" type="month" id="fecha" placeholder="Ingrese aqui la fecha" name="fecha" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="archivo" class=" form-control-label">Archivo</label>
                                            <input required="" type="file" id="archivo" placeholder="Ingrese aqui el archivo" name="archivo" class="form-control">
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                                <i class="fa fa-chevron-left"></i>
                                                Cerrar
                                            </button>
                                            <button class="btn btn-primary">
                                                <i class="fa fa-save"></i>
                                                Guardar
                                            </button>
                                        </div>
                                    </form>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <button type="button" class="au-btn au-btn-icon au-btn--blue au-btn--small" data-toggle="modal" data-target="#agregarLiquidacion">
                                <i class="fa fa-plus"></i>
                                Liquidaciones
                            </button>

                            <!-- Modal -->
                            <div class="modal fade" id="agregarLiquidacion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Agregar archivo</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <div class="modal-body">
                                    <form action="{{url('liquidacion')}}" method="post" class="form-horizontal" enctype="multipart/form-data">
                                        @csrf
                                        <div class="form-group">
                                            <label for="fecha" class=" form-control-label">Fecha</label>
                                            <input required="" type="month" id="fecha" placeholder="Ingrese aqui la fecha" name="fecha" class="form-control" value="{{Carbon\Carbon::now()->subMonth()->format('Y-m')}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="edificio" class=" form-control-label">Edificio</label>
                                            <select name="edificio" id="" class="form-control">
                                                @foreach(App\Models\Edificios::all()->sortBy('codigo') as $edificio)
                                                    <option value="{{$edificio->id}}">{{$edificio->nombre}}</option>}
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="archivo" class=" form-control-label">Archivo</label>
                                            <input required="" type="file" id="archivo" placeholder="Ingrese aqui el archivo" name="archivo" class="form-control">
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                                <i class="fa fa-chevron-left"></i>
                                                Cerrar
                                            </button>
                                            <button class="btn btn-primary">
                                                <i class="fa fa-save"></i>
                                                Guardar
                                            </button>
                                        </div>
                                    </form>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <button type="button" class="au-btn au-btn-icon au-btn--blue au-btn--small" data-toggle="modal" data-target="#agregarDocumento">
                                <i class="fa fa-plus"></i>
                                Documentos
                            </button>

                            <!-- Modal -->
                            <div class="modal fade" id="agregarDocumento" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Agregar archivo</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <div class="modal-body">
                                    <form action="{{url('documento')}}" method="post" class="form-horizontal" enctype="multipart/form-data">
                                        @csrf
                                        <div class="form-group">
                                            <label for="edificio" class=" form-control-label">Edificio</label>
                                            <select name="edificio" id="" class="form-control">
                                                @foreach(App\Models\Edificios::all()->sortBy('codigo') as $edificio)
                                                    <option value="{{$edificio->id}}">{{$edificio->nombre}}</option>}
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Tipo</label>
                                            <select name="tipo" id="tipo" class="form-control">
                                                <option value="acta">Acta</option>
                                                <option value="reglamento">Reglamento</option>
                                                <option value="poliza">Poliza</option>
                                                <option value="comprobante">Comprobante</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="archivo" class=" form-control-label">Archivo</label>
                                            <input required="" type="file" id="archivo" placeholder="Ingrese aqui el archivo" name="archivo" class="form-control">
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                                <i class="fa fa-chevron-left"></i>
                                                Cerrar
                                            </button>
                                            <button class="btn btn-primary">
                                                <i class="fa fa-save"></i>
                                                Guardar
                                            </button>
                                        </div>
                                    </form>
                                  </div>
                                </div>
                              </div>
                            </div>

                        	<button type="button" class="au-btn au-btn-icon au-btn--blue au-btn--small" data-toggle="modal" data-target="#agregarEdificio">
							  	<i class="fa fa-plus"></i>
							  	Edificio
							</button>

							<!-- Modal -->
							<div class="modal fade" id="agregarEdificio" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							  <div class="modal-dialog" role="document">
							    <div class="modal-content">
							      <div class="modal-header">
							        <h5 class="modal-title" id="exampleModalLabel">Agregar edificio</h5>
							        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
							          <span aria-hidden="true">&times;</span>
							        </button>
							      </div>
							      <div class="modal-body">
							        <form action="{{url('edificio')}}" method="post" class="form-horizontal">
							        	@csrf
							        	<div class="form-group">
                                            <label for="codigo" class=" form-control-label">Codigo</label>
                                            <input required="" type="text" id="codigo" placeholder="Ingrese aqui el codigo" name="codigo" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="nombre" class=" form-control-label">Nombre</label>
                                            <input required="" type="text" id="nombre" placeholder="Ingrese aqui el nombre" name="nombre" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="direccion" class=" form-control-label">Dirección</label>
                                            <input type="text" id="direccion" placeholder="Ingrese aqui el dirección" name="direccion" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="contrasena" class=" form-control-label">Contraseña</label>
                                            <div class="row">
                                                <div class="col-8">
                                                    <input type="text" id="contrasena" value="123456" name="contrasena" class="form-control">
                                                </div>
                                                <div class="col-4">
                                                    <a class="btn btn-info btn_contrasena" style="color:white;">
                                                        <i class="fa fa-key"></i>
                                                        Aleatoria
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="contrasena_admin" class=" form-control-label">Contraseña administración</label>
                                            <div class="row">
                                                <div class="col-8">
                                                    <input type="text" id="contrasena_admin" value="123456" name="contrasena_admin" class="form-control">
                                                </div>
                                                <div class="col-4">
                                                    <a class="btn btn-info btn_contrasena_admin" style="color:white;">
                                                        <i class="fa fa-key"></i>
                                                        Aleatoria
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
									        <button type="button" class="btn btn-secondary" data-dismiss="modal">
									        	<i class="fa fa-chevron-left"></i>
									        	Cerrar
									        </button>
									        <button class="btn btn-primary">
									        	<i class="fa fa-save"></i>
									        	Guardar
									        </button>
								      	</div>
							        </form>
							      </div>
							    </div>
							  </div>
							</div>
                        </div>
                    </div>
                        <hr>
                    @include('ayuda.alerta')

                    @if($edificios->count() == 0)
                        <div class="row">
                            <div class="col-12 text-center">
                                <h4>No hay ningun edificio registrado</h4>
                            </div>
                        </div>
                    @else
                        <div class="table-responsive table-responsive-data2">
                            <table class="table table-data2">
                                <thead>
                                    <tr>
                                        <th>Codigo</th>
                                        <th>Nombre</th>
                                        <th>Dirección</th>
                                        <th>Contraseña</th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($edificios->sortBy('codigo') as $edificio)
                                        <tr class="tr-shadow">
                                            <td>{{$edificio->codigo}}</td>
                                            <td>{{$edificio->nombre}}</td>
                                            <td>{{$edificio->direccion}}</td>
                                            <td>{{$edificio->contrasena}}</td>
                                            <td>
                                                <a href="{{url('edificio', $edificio->id)}}" class="btn btn-success">
                                                    <i class="fa fa-barcode"></i>    
                                                    Recibos
                                                </a>
                                            </td>
                                            <td>
                                                <a href="{{url('liquidacion', $edificio->id)}}" class="btn btn-primary">
                                                    <i class="fa fa-file-alt"></i>
                                                    Liquidaciones
                                                </a>
                                            </td> 
                                            <td>
                                                <a href="{{url('documento', $edificio->id)}}" class="btn btn-warning">
                                                    <i class="fa fa-folder"></i>
                                                    Documentos
                                                </a>
                                            </td>   
                                            <td>
                                                <button class="btn btn-info" data-toggle="modal" data-target="#editarEdificio{{$edificio->id}}">
                                                    <i class="fa fa-edit"></i>
                                                    Editar
                                                </button>

                                                <!-- Modal -->
                                                <div class="modal fade" id="editarEdificio{{$edificio->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                  <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                      <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Editar edificio</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                          <span aria-hidden="true">&times;</span>
                                                        </button>
                                                      </div>
                                                      <div class="modal-body">
                                                        <form action="{{url('edificio', $edificio->id)}}" method="post" class="form-horizontal">
                                                            @csrf
                                                            @method('PATCH')
                                                            <div class="form-group">
                                                                <label for="codigo" class=" form-control-label">Codigo</label>
                                                                <input required="" type="text" id="codigo" value="{{$edificio->codigo}}" name="codigo" class="form-control">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="nombre" class=" form-control-label">Nombre</label>
                                                                <input required="" type="text" id="nombre" value="{{$edificio->nombre}}" name="nombre" class="form-control">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="direccion" class=" form-control-label">Dirección</label>
                                                                <input type="text" id="direccion" value="{{$edificio->direccion}}" name="direccion" class="form-control">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="contrasena" class=" form-control-label">Contraseña</label>
                                                                <div class="row">
                                                                    <div class="col-8">
                                                                        <input type="text" id="contrasena_{{$edificio->id}}" value="{{$edificio->contrasena}}" name="contrasena" class="form-control">
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <a class="btn btn-info btn_contrasena_{{$edificio->id}}" style="color:white;">
                                                                            <i class="fa fa-key"></i>
                                                                            Aleatoria
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="contrasena" class=" form-control-label">Contraseña Administración</label>
                                                                <div class="row">
                                                                    <div class="col-8">
                                                                        <input type="text" id="contrasena_admin_{{$edificio->id}}" value="{{$edificio->contrasena_admin}}" name="contrasena_admin" class="form-control">
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <a class="btn btn-info btn_contrasena_admin_{{$edificio->id}}" style="color:white;">
                                                                            <i class="fa fa-key"></i>
                                                                            Aleatoria
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                                                    <i class="fa fa-chevron-left"></i>
                                                                    Cerrar
                                                                </button>
                                                                <button class="btn btn-primary">
                                                                    <i class="fa fa-save"></i>
                                                                    Guardar
                                                                </button>
                                                            </div>
                                                        </form>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                            </td>
                                            <td>
                                                @include('ayuda.eliminar', ['id' => $edificio->id, 'ruta' => url('edificio', $edificio->id)])
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </section>
    <!-- END DATA TABLE-->
@endsection

@section('js')
	<script>
		$(document).ready(function()
        {
            $('#agregarEdificio').on('shown.bs.modal', function () {
              $('#codigo').trigger('focus');
            });
            
            $('.btn_contrasena').click(function()
            {
                $('#contrasena').val(generatePasswordRand(8, 'num'));
            });

            $('.btn_contrasena_admin').click(function()
            {
                $('#contrasena_admin').val(generatePasswordRand(8, 'num'));
            });

            @foreach(App\Models\Edificios::all() as $edificio)
                $('.btn_contrasena_{{$edificio->id}}').click(function()
                {
                    $('#contrasena_{{$edificio->id}}').val(generatePasswordRand(8, 'num'));
                });

                $('.btn_contrasena_admin_{{$edificio->id}}').click(function()
                {
                    $('#contrasena_admin_{{$edificio->id}}').val(generatePasswordRand(8, 'num'));
                });
            @endforeach


            function generatePasswordRand(length,type) {
                switch(type){
                    case 'num':
                        characters = "0123456789";
                        break;
                    case 'alf':
                        characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
                        break;
                    case 'rand':
                        //FOR ↓
                        break;
                    default:
                        characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                        break;
                }
                var pass = "";
                for (i=0; i < length; i++){
                    if(type == 'rand'){
                        pass += String.fromCharCode((Math.floor((Math.random() * 100)) % 94) + 33);
                    }else{
                        pass += characters.charAt(Math.floor(Math.random()*characters.length));   
                    }
                }
                return pass;
            }
        });
	</script>			
@endsection