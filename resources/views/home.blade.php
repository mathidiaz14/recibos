@extends('layouts.app')

@section('content')


    <!-- WELCOME-->
    <section class="welcome p-t-10">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="title-4">Bienvenido/a
                        <span>{{Auth::user()->name}}</span>
                    </h1>
                    <hr class="line-seprate">
                </div>
            </div>
        </div>
    </section>
    <!-- END WELCOME-->

    <!-- STATISTIC-->
    <section class="statistic statistic2">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-lg-4">
                    <div class="statistic__item statistic__item--green">
                        <h2 class="number">{{App\Models\Edificios::count()}}</h2>
                        <span class="desc">Edificios</span>
                        <div class="icon">
                            <i class="fas fa-building"></i>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="statistic__item statistic__item--blue">
                        <h2 class="number">{{App\Models\Enlaces::count()}}</h2>
                        <span class="desc">Recibos</span>
                        <div class="icon">
                            <i class="fas fa-barcode"></i>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="statistic__item statistic__item--red">
                        <h2 class="number">{{App\Models\Enlaces::all()->sum('descargado')}}</h2>
                        <span class="desc">Recibos descargados</span>
                        <div class="icon">
                            <i class="fas fa-download"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END STATISTIC-->
@endsection
