<button class="btn btn-danger" data-toggle="modal" data-target="#eliminarItem{{$id}}">
    <i class="fa fa-trash"></i>
    Eliminar
</button>

<!-- Modal -->
<div class="modal fade" id="eliminarItem{{$id}}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true" >
	<div class="modal-dialog" role="document">
  		<div class="modal-content">
			<div class="modal-header" style="background: #dc3545;">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
    		</div>
    		<div class="modal-body text-center">
    			<p><i class="fa fa-exclamation-triangle fa-4x"></i></p>
    			<br>
      			<h4>¿Desea eliminar el item?</h4>
      			<br>
      			<div class="row">
      				<div class="col">
						<button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">
							NO
						</button>
      				</div>
      				<div class="col">
      					<form action="{{ $ruta }}" method="POST">
                    @csrf
                    <input type='hidden' name='_method' value='DELETE'>
                    <button class="btn btn-danger btn-block">
                        SI
                    </button>
                </form>
      				</div>
      			</div>
    		</div>
  		</div>
	</div>
</div>