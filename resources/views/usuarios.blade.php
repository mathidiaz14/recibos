@extends('layouts.app')

@section('content')

    <section class="p-t-20">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="table-data__tool">
                    	<div class="table-data__tool-left">
                    		<h3 class="title-5 m-b-35">Usuarios registrados</h3>
                    	</div>
                        <div class="table-data__tool-right">
                        	<button type="button" class="au-btn au-btn-icon au-btn--green au-btn--small" data-toggle="modal" data-target="#agregarUsuario">
							  <i class="fa fa-plus"></i>
							  Agregar usuario
							</button>

							<!-- Modal -->
							<div class="modal fade" id="agregarUsuario" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							  <div class="modal-dialog" role="document">
							    <div class="modal-content">
							      <div class="modal-header">
							        <h5 class="modal-title" id="exampleModalLabel">Agregar Usuario</h5>
							        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
							          <span aria-hidden="true">&times;</span>
							        </button>
							      </div>
							      <div class="modal-body">
							        <form action="{{url('usuario')}}" method="post" class="form-horizontal">
							        	@csrf
							        	<div class="form-group">
                                            <label for="nombre" class=" form-control-label">Nombre</label>
                                            <input required="" type="text" id="nombre" placeholder="Ingrese aqui el nombre" name="nombre" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="email" class=" form-control-label">Dirección de correo</label>
                                            <input required="" type="email" id="email" placeholder="Ingrese aqui el email" name="email" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="contraseña" class=" form-control-label">Contraseña</label>
                                            <input required="" type="text" id="contraseña" placeholder="Ingrese aqui la contraseña" name="contraseña" class="form-control">
                                        </div>
										<div class="modal-footer">
									        <button type="button" class="btn btn-secondary" data-dismiss="modal">
									        	<i class="fa fa-chevron-left"></i>
									        	Cerrar
									        </button>
									        <button class="btn btn-primary">
									        	<i class="fa fa-save"></i>
									        	Guardar
									        </button>
								      	</div>
							        </form>
							      </div>
							    </div>
							  </div>
							</div>
                        </div>
                    </div>
                    @include('ayuda.alerta')
                    <div class="table-responsive table-responsive-data2">
                        <table class="table table-data2">
                            <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Email</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach(Auth::user()->all() as $usuario)
                                	<tr class="tr-shadow">
	                                    <td>{{$usuario->name}}</td>
	                                    <td>
	                                        <span class="block-email">{{$usuario->email}}</span>
	                                    </td>
	                                    <td>
                                            <button class="btn btn-info" data-toggle="modal" data-target="#editarUsuario{{$usuario->id}}">
                                                <i class="fa fa-edit"></i>
                                                Editar
                                            </button>

                                            <!-- Modal -->
                                            <div class="modal fade" id="editarUsuario{{$usuario->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                              <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                  <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Editar Usuario</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                      <span aria-hidden="true">&times;</span>
                                                    </button>
                                                  </div>
                                                  <div class="modal-body">
                                                    <form action="{{url('usuario', $usuario->id)}}" method="post" class="form-horizontal">
                                                        @csrf
                                                        @method('PATCH')
                                                        <div class="form-group">
                                                            <label for="nombre" class=" form-control-label">Nombre</label>
                                                            <input required="" type="text" id="nombre" value="{{$usuario->name}}" name="nombre" class="form-control">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="email" class=" form-control-label">Dirección de correo</label>
                                                            <input required="" type="email" id="email" value="{{$usuario->email}}"name="email" class="form-control">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="contraseña" class=" form-control-label">Contraseña</label>
                                                            <small>(Si no completa el campo, la contraseña no cambiara.)</small>
                                                            <input type="text" id="contraseña" placeholder="Ingrese aqui la contraseña si desea cambiarla" name="contraseña" class="form-control">
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                                                <i class="fa fa-chevron-left"></i>
                                                                Cerrar
                                                            </button>
                                                            <button class="btn btn-primary">
                                                                <i class="fa fa-save"></i>
                                                                Guardar
                                                            </button>
                                                        </div>
                                                    </form>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                        </td>
                                        <td>
                                            @include('ayuda.eliminar', ['id' => $usuario->id, 'ruta' => url('usuario', $usuario->id)])
	                                    </td>
	                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END DATA TABLE-->
@endsection