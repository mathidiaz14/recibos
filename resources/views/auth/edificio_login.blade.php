@extends('layouts.auth')

@section('content')
<div class="login-wrap">
    <div class="login-content">
        <div class="login-logo">
            <h1><b>{{env('APP_TITULO')}}</b></h1>
        </div>
        <hr>
        @if(Session::has('error'))
        	<span role="alert" style="  color: #721c24;
                                        background-color: #f8d7da;
                                        border-color: #f5c6cb;
                                        padding: .75rem 1.25rem;
                                        margin-bottom: 1rem;
                                        border: 1px solid transparent;
                                        border-radius: .25rem;
                                        width: 100%;">
                <strong>{{Session::get('error')}}</strong>
                {{Session::forget('error')}}
            </span>
            <br><br>
        @endif
        <div class="login-form">
            <form method="POST" action="{{ url('edificio/login') }}">
                @csrf
                <div class="form-group">
                    <label>Seleccionar edificio</label>
                    <select name="edificio" id="" class="form-control">
                    	@foreach(App\Models\Edificios::where('contrasena_admin', '!=', null)->get() as $edificio)
                    		<option value="{{$edificio->id}}">{{$edificio->nombre}}</option>
                    	@endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>Contraseña</label>
                    <input class="au-input au-input--full" type="password" name="password" placeholder="Contraseña">
                </div>
                <hr>
                <button class="au-btn au-btn--block au-btn--green m-b-20" type="submit">Iniciar Sesión</button>
            </form>
        </div>
    </div>
</div>

@endsection
