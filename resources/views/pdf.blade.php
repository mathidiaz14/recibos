<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>{{env('APP_TITULO')}}</title>
    <style>
    	table, th, td {
		  border: 1px solid black;
		}
    </style>	
</head>

<body>
    <br><br>
	<h3>Reservas edificio: {{$edificio->nombre}}</h3>
	<hr>
	<br>
	<table class="table table-striped" style="width:100%;">
		<tr>
            <th>Fecha</th>
			<th>Turno</th>
			<th>Unidad</th>
			<th>Nombre</th>
			<th>Telefono</th>
			<th>Estado</th>
            <th>Comentario</th>
		</tr>
		@foreach($reservas as $reserva)
			<tr>
				<td>{{$reserva->fecha->format('d/m/Y')}}</td>
				<td>{{$reserva->inicio == "matutino" ? "Diurno" : "Nocturno"}}</td>
				<td>{{$reserva->unidad}}</td>
				<td>{{$reserva->nombre}}</td>
				<td>{{$reserva->telefono}}</td>
				<td>{{$reserva->estado}}</td>
                <td>{{$reserva->comentario == null ? "--" : $reserva->comentario}}</td>
			</tr>
		@endforeach
	</table>
</body>

</html>
<!-- end document-->

