<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ControladorEdificios;
use App\Http\Controllers\ControladorLiquidaciones;
use App\Http\Controllers\ControladorRecibos;
use App\Http\Controllers\ControladorUsuarios;
use App\Http\Controllers\ControladorDocumentos;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\Auth\ResetPasswordController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/'                              , [HomeController::class, 'landing']);
Route::get('/home'                          , [HomeController::class, 'index'])->name('home')->middleware('auth');

Route::get('buscar'                         , [HomeController::class, 'buscar']);
Route::post('menu'                        	, [HomeController::class, 'contrasena']);
Route::post('get_menu'                    	, [HomeController::class, 'get_menu']);
Route::post('fechas'                        , [HomeController::class, 'get_fechas']);
Route::post('get_documentos'				, [HomeController::class, 'get_documentos']);
Route::post('get_reserva'					, [HomeController::class, 'get_reserva']);
Route::post('reserva'						, [HomeController::class, 'set_reserva']);
Route::get('descargar_documento/{id}'     	, [HomeController::class, 'descargar_documento']);
Route::get('descargar_liquidacion/{id}'     , [HomeController::class, 'descargar_liquidacion']);


Route::get('edificio/login'					, [ControladorEdificios::class, 'get_login']);
Route::post('edificio/login'				, [ControladorEdificios::class, 'login']);
Route::post('reserva/aprobar/{id}'			, [ControladorEdificios::class, 'aprobar']);
Route::post('reserva/rechazar/{id}'			, [ControladorEdificios::class, 'rechazar']);
Route::post('pdf'							, [ControladorEdificios::class, 'pdf']);

Route::resource('edificio'                  , ControladorEdificios::class)->middleware('auth');

Route::resource('recibo'                    , ControladorRecibos::class)->middleware('auth');
Route::delete('enlace/{id}'                 , [ControladorRecibos::class, 'eliminar_enlace'])->middleware('auth');

Route::resource('usuario'                   , ControladorUsuarios::class)->middleware('auth');
Route::resource('liquidacion'               , ControladorLiquidaciones::class)->middleware('auth');
Route::resource('gasto'               		, ControladorLiquidaciones::class)->middleware('auth');
Route::resource('documento'           		, ControladorDocumentos::class)->middleware('auth');


Auth::routes();
