<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Edificios;
use Hash;

class GenerarContraseñaEdificio extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generar_contrasenas';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('Inicia el comando');
        $this->info('*****************');
        $this->newLine();

        $edificios = Edificios::all();

        $bar = $this->output->createProgressBar(count($edificios));

        $bar->start();

        foreach($edificios as $edificio)
        {
            $pass = $edificio->contrasena.".2023";

            $edificio->contrasena_admin = $pass;
            $edificio->save();

            $bar->advance();
        }

        $bar->finish();
        
        $this->newLine();        
        $this->info("*******************");
        $this->info("Finaliza el proceso");
    }
}
