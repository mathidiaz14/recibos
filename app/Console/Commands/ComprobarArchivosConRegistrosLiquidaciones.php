<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Liquidaciones;

class ComprobarArchivosConRegistrosLiquidaciones extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'liquidaciones:archivos';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Comando que chequea que cada liquidacion tenga un archivo PDF existente';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $liquidaciones  = Liquidaciones::all();
        $sin_archivo    = collect();

        foreach($liquidaciones as $liquidacion)
        {
            if(!file_exists(storage_path('app')."/".$liquidacion->archivo))
                $sin_archivo->add($liquidacion->id);
        }

        if($sin_archivo->count() == 0)
            $this->info('Todas las liquidaciones tienen un archivo asociado');
        else
            $this->error('Las liquidaciones sin archivo son:');

        foreach($sin_archivo as $i)
            $this->error($i);

    }
}
