<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Edificios;
use App\Models\Liquidaciones;

class ControladorLiquidaciones extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        set_time_limit(240);

        $archivo = $request->file('archivo');

        //obtenemos el nombre del archivo
        $nombre = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 30).".pdf";

        //indicamos que queremos guardar un nuevo archivo en el disco local
        \Storage::disk('local')->put($nombre,  \File::get($archivo));

        $edificio = Edificios::find($request->edificio);

        if($edificio->liquidaciones->where('fecha', $request->fecha)->first() == null)
        {
            $liquidacion = new Liquidaciones();
            $liquidacion->edificios_id = $request->edificio;
            $liquidacion->archivo = $nombre;
            $liquidacion->fecha = $request->fecha;
            $liquidacion->save();
            
            Session(['exito' => "Se cargo la liquidación de la fecha ".$liquidacion->fecha." para el edificio ".$liquidacion->edificio->nombre]);
        }else
        {
            Session(['error' => "El edificio ya tiene la liquidacion para ese mes."]);
        }

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $edificio = Edificios::find($id);

        if($edificio != null)
            return view('liquidaciones', compact('edificio'));

        echo('El edificio no existe');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $liquidacion = Liquidaciones::find($id);
        $liquidacion->delete();

        return back();
    }
}
