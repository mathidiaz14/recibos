<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Recibos;
use App\Models\Edificios;
use App\Models\Enlaces;
use App\Models\Configuracion;
use Redirect;
use Storage;
use File;

class ControladorRecibos extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('recibos');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        set_time_limit(240);

        $archivo = $request->file('archivo');

        //obtenemos el nombre del archivo
        $nombre = $archivo->getClientOriginalName();

        //indicamos que queremos guardar un nuevo archivo en el disco local
        \Storage::disk('local')->put($nombre,  \File::get($archivo));

        $filename = storage_path('app/'.$nombre);
        $contenido = explode("\r\n", \File::get($filename));

        foreach($contenido as $cont)
        {
            $linea      = explode('|', $cont);
            
            $edificio   = Edificios::where('codigo', $linea[0])->first();

            if ($edificio != null)
            {
                if($edificio->recibos->where('fecha', $request->fecha)->first() == null)
                {
                    $recibo = new Recibos();
                    $recibo->fecha = $request->fecha;
                    $recibo->edificios_id = $edificio->id;
                    $recibo->save();
                }else
                {
                    $recibo = $edificio->recibos->where('fecha', $request->fecha)->first();
                }

                if($recibo->enlaces->where('unidad', $linea[3])->first() == null)
                {
                    $enlace             = new Enlaces();
                    $enlace->recibos_id = $recibo->id;
                    $enlace->unidad     = $linea[3];
                    $enlace->link       = $linea[6];
                    $enlace->save();
                }
            }
        }

        File::delete(storage_path('app/'.$nombre));
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $recibo = Recibos::find($id);
        $enlaces = Enlaces::where('recibos_id', $recibo->id)->paginate(1000);

        return view('enlaces', compact('recibo', 'enlaces'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $recibo = Recibos::find($id);
        
        foreach($recibo->enlaces as $enlace)
            $enlace->delete();

        $recibo->delete();

        return back();
    }

    public function eliminar_enlace($id)
    {
        $enlace = Enlaces::find($id);
        $enlace->delete();

        return back(); 
    }
}
