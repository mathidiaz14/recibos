<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Edificios;
use App\Models\Reserva;
use Carbon\Carbon;
use PDF;

class ControladorEdificios extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $edificios = Edificios::all();

        return view('edificios', compact('edificios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $edificio = new Edificios();
        $edificio->codigo = $request->codigo;
        $edificio->nombre = $request->nombre;
        $edificio->direccion = $request->direccion;
        $edificio->contrasena = $request->contrasena;
        $edificio->contrasena_admin = $request->contrasena_admin;
        $edificio->save();

        Session(['exito' => "El edificio se agrego correctamente."]);
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $edificio = Edificios::find($id);

        return view('recibos_edificio', compact('edificio'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $edificio = Edificios::find($id);
        $edificio->codigo = $request->codigo;
        $edificio->nombre = $request->nombre;
        $edificio->direccion = $request->direccion;
        $edificio->contrasena = $request->contrasena;
        $edificio->contrasena_admin = $request->contrasena_admin;
        $edificio->save();

        Session(['exito' => "El edificio se modifico correctamente."]);
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $edificio = Edificios::find($id);
        $edificio->delete();

        Session(['exito' => "El edificio se elimino correctamente."]);
        return back();
    }

    public function get_login()
    {
        return view('auth.edificio_login');
    }

    public function login(Request $request)
    {
        $edificio = Edificios::find($request->edificio);

        if($edificio->contrasena_admin == $request->password)
            return view('dashboard', compact('edificio'));

        session(['error' => 'La contraseña no es correcta, intentelo nuevamente']);
        return back();
    }

    public function aprobar($id, Request $request)
    {
        $reserva = Reserva::find($id);

        $reserva->estado = "aprobado";
        $reserva->save();

        $contenido = "La reserva para la fecha ".$reserva->fecha->format('d/m/Y')." turno ".$reserva->inicio.", fue aprobada por el administrador del edificio.";

        enviar_email("aprobado", "Reserva aprobada", $contenido, $reserva->nombre, $reserva->email);

        session(['exito' => "La reserva se aprobo correctamente"]);

        $edificio = $reserva->edificio;
        return view('dashboard', compact('edificio'));
    }

    public function rechazar($id, Request $request)
    {
        $reserva                = Reserva::find($id);
        $reserva->estado        = "rechazado";
        $reserva->comentario    = $request->comentario;
        $reserva->save();

        $contenido = "La reserva para la fecha ".$reserva->fecha->format('d/m/Y')." turno ".$reserva->inicio.", fue rechazada por el administrador del edificio.<br><br>".$request->comentario;

        enviar_email("rechazado", "Reserva rechazada", $contenido, $reserva->nombre, $reserva->email);

        session(['exito' => "La reserva se rechazo correctamente"]);
        
        $edificio = $reserva->edificio;
        return view('dashboard', compact('edificio'));
    }

    public function pdf(Request $request)
    {
        $edificio   = Edificios::find($request->edificio);
        
        $inicio     = Carbon::create($request->inicio);
        $fin        = Carbon::create($request->fin);

        if($request->estado == "todos")
            $reservas   = $edificio->reservas->whereBetween('fecha', [$inicio, $fin]);
        else
            $reservas   = $edificio->reservas->whereBetween('fecha', [$inicio, $fin])->where('estado', $request->estado);

        //return view ('pdf', compact('reservas', 'edificio'));
        $pdf = PDF::loadView('pdf', compact('reservas', 'edificio'))->output();
        
        return response()->streamDownload(
             fn () => print($pdf),
             $edificio->nombre.".pdf"
        );
    }
}
