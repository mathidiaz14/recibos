<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Edificios;
use App\Models\Liquidaciones;
use App\Models\Documentos;
use App\Models\Reserva;
use Carbon\Carbon;
use Hash;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $edificios = Edificios::all();

        return view('edificios', compact('edificios'));
    }

    public function landing()
    {
        return view('landing.'.env('APP_EMPRESA').'.index');
    }

    public function buscar(Request $request)
    {
        $edificios = Edificios::where('nombre', 'like', '%'.$request->edificio.'%')->orWhere('direccion', 'like', '%'.$request->edificio.'%')->get();
        
        return view('landing.'.env('APP_EMPRESA').'.busqueda', compact('edificios'));
    }

    public function contrasena(Request $request)
    {
        $edificio = Edificios::find($request->edificio);

        if($edificio->contrasena == null)
        {
            return view('landing.'.env('APP_EMPRESA').'.menu', compact('edificio'));
        }else
        {
            if($request->contrasena == $edificio->contrasena)
            {
                return view('landing.'.env('APP_EMPRESA').'.menu', compact('edificio'));
            }
            else
            {
                Session(["contrasena" => "Has colocado una contraseña incorrecta, por favor, prueba nuevamente o comunicate con la administración."]);

                return back();
            }
        }
    }

    public function get_menu(Request $request)
    {
        $edificio = Edificios::find($request->edificio);
        return view('landing.'.env('APP_EMPRESA').'.menu', compact('edificio'));   
    }

    public function get_fechas(Request $request)
    {
        $edificio = Edificios::find($request->edificio);
        return view('landing.'.env('APP_EMPRESA').'.fechas', compact('edificio'));
    }

    public function get_documentos(Request $request)
    {
        $edificio = Edificios::find($request->edificio);
        return view('landing.'.env('APP_EMPRESA').'.documentos', compact('edificio'));
    }

    public function get_reserva(Request $request)
    {
        $edificio = Edificios::find($request->edificio);
        return view('landing.'.env('APP_EMPRESA').'.reserva', compact('edificio'));
    }

    public function set_reserva(Request $request)
    {
        $edificio               = Edificios::find($request->edificio);

        $reserva                = new Reserva();
        $reserva->fecha         = $request->fecha;
        $reserva->inicio        = $request->inicio;
        $reserva->fin           = "--";
        $reserva->unidad        = $request->unidad;
        $reserva->nombre        = $request->nombre;
        $reserva->email         = $request->email;
        $reserva->telefono      = $request->telefono;
        $reserva->estado        = "pendiente";
        $reserva->edificios_id  = $edificio->id;
        $reserva->save();

        $contenido = "Se realizo una reserva para la fecha ".$reserva->fecha->format('d/m/Y')."en el edificio ".$edificio->nombre."<br><br>Ingresa aquí para ver la reserva <a href=".url('edificio/login').">".url('edificio/login')."</a>";

        enviar_email("reserva", "Nueva reserva edificio ".$edificio->nombre, $contenido, env('APP_TITULO'), env('APP_EMAIL'));

        session(['exito' => "La reserva se agendo correctamente, debera ser aprobada por el encargado del edificio"]);
        
        return view('landing.'.env('APP_EMPRESA').'.reserva', compact('edificio'));
    }

    public function consulta_fecha($edificio, $fecha)
    {
        $edificio               = Edificios::find($edificio);

        $fechas_reservadas      = $edificio->reservas->where('fecha', $fecha)->where('estado', 'aprobado');
        $fechas_pendientes      = $edificio->reservas->where('fecha', $fecha)->where('estado', 'pendiente');
        
        $reservadas             = collect();
        $pendientes             = collect();

        foreach($fechas_reservadas as $fecha)
        {
            $inicio   = Carbon::create($fecha->inicio);
            $fin      = Carbon::create($fecha->fin);

            do
            {
                $reservadas->push($inicio->format('d-m-Y_Hi'));

                $inicio->addMinutes(30);
            
            }while($inicio->diffInMinutes($fin) > 0);
        }

        foreach($fechas_pendientes as $fecha)
        {
            $inicio   = Carbon::create($fecha->inicio);
            $fin      = Carbon::create($fecha->fin);

            do
            {
                $pendientes->push($inicio->format('d-m-Y_Hi'));

                $inicio->addMinutes(30);
            
            }while($inicio->diffInMinutes($fin) > 0);
        }

        return [$reservadas, $pendientes];
    }

    public function descargar_liquidacion($id)
    {
        $liquidacion = Liquidaciones::find($id);

        return response()->file(storage_path('app/'.$liquidacion->archivo));
    }

    public function descargar_documento($id)
    {
        $documento = Documentos::find($id);

        return response()->file(storage_path('app/'.$documento->archivo));
    }
}

