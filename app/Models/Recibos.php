<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Recibos extends Model
{
    public function edificio()
    {
        return $this->belongsTo('App\Models\Edificios', 'edificios_id');
    }

    public function enlaces()
    {
        return $this->hasMany('App\Models\Enlaces');
    }
}
