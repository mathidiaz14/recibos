<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reserva extends Model
{
    use HasFactory;

    protected $dates = ['fecha'];
    
    public function edificio()
    {
        return $this->belongsTo('App\Models\Edificios', 'edificios_id');
    }
}
