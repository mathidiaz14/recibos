<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Enlaces extends Model
{
    public function recibo()
    {
        return $this->belongsTo('App\Models\Recibos');
    }
}
