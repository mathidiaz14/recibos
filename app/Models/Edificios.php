<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Edificios extends Model
{
    public function recibos()
    {
        return $this->hasMany('App\Models\Recibos');
    }

    public function liquidaciones()
    {
        return $this->hasMany('App\Models\Liquidaciones');
    }

    public function gastos()
    {
        return $this->hasMany('App\Models\Gastos');
    }

    public function documentos()
    {
        return $this->hasMany('App\Models\Documentos');
    }

    public function reservas()
    {
        return $this->hasMany('App\Models\Reserva');
    }
}
