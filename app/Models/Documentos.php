<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Documentos extends Model
{
    use HasFactory;

    public function edificio()
    {
        return $this->belongsTo('App\Models\Edificios', 'edificios_id');
    }
}
