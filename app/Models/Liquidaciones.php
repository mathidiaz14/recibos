<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Liquidaciones extends Model
{
    public function edificio()
    {
        return $this->belongsTo('App\Models\Edificios', 'edificios_id');
    }
}
